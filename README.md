# Users-list

- Data imported by running a request for it in the application in the browser,
- The list of contacts sorted alphabetically by last_name,
- The page display a list of contacts in the following format:
  {avatar} | {first_name last_name} | {checkbox},
- When a user clicks on a contact (list item) the checkbox toggle on/off and you
  should console.log IDs of all selected contacts.,
- Added one text input at the top of the list that allows you to filter the contacts by
  first_name and last_name. Searching do not cause contacts to become
  unchecked.

## Live

(lebron22.github.io/material-ui-list/)

## Technologies

- ReactJS + Typescript
- React-Query
- Material-UI

## Setup

For running this project on your local machine:

- [Node.js](https://nodejs.org/en/download/) is required.

- Clone repo from [master branch](https://github.com/lebron22/car-map/)

- Then execute following commands on your terminal in the project directory:

```bash
$ npm install
```

- Next you can run the application on your localhost:

```bash
$ npm start
```

- Build for production:

```bash
$ npm run build
```
