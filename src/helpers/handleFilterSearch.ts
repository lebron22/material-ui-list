export const handleFilterSearch = (
  itemsToSearchInArr: any[],
  searchInputValue: string
): number => {
  const res = itemsToSearchInArr.filter((itemToSearchIn) =>
    itemToSearchIn?.toLowerCase().includes(searchInputValue.toLowerCase())
  );
  return res.length;
};
