import React, { useState } from "react";
import { useQuery } from "react-query";
import Error from "./components/Error/Error";
import Loading from "./components/Loading/Loading";
import Searchbar from "./components/Searchbar/Searchbar";
import UserList from "./components/UserList/UserList";
import { Container } from "@material-ui/core";

const fetchData = async () => {
  const res = await fetch(
    "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
  );
  return res.json();
};

const App: React.FC = () => {
  const [searchInputValue, setSearchInputValue] = useState<string>("");

  const { data, status } = useQuery("people", fetchData);

  return (
    <>
      <Searchbar
        searchInputValue={searchInputValue}
        setSearchInputValue={setSearchInputValue}
      />

      <Container maxWidth={false}>
        {status === "error" && <Error />}

        {status === "loading" && <Loading />}

        {status === "success" && (
          <UserList data={data} searchInputValue={searchInputValue} />
        )}
      </Container>
    </>
  );
};

export default App;
