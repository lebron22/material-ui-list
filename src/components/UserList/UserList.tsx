import React, { useState } from "react";
import { IUserObject } from "../../interfaces";
import List from "@material-ui/core/List";
import SingleUser from "../SingleUser/SingleUser";
import { getStyles } from "./getStyles";
import { handleFilterSearch } from "../../helpers/handleFilterSearch";
import ListPagination from "../ListPagination/ListPagination";
import { POSTS_PER_PAGE } from "../../constants/constants";
export interface UserListProps {
  data: IUserObject[];
  searchInputValue: string;
}

const UserList: React.FC<UserListProps> = ({ data, searchInputValue }) => {
  const [checked, setChecked] = useState<number[]>([]);
  const [page, setPage] = React.useState(1);

  const classes = getStyles();

  const handlePageChange = (
    event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    setPage(value);
  };

  const handleToggle = (id: number) => () => {
    const currentIndex = checked.indexOf(id);
    const newChecked = [...checked];
    if (currentIndex === -1) {
      newChecked.push(id);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    //TODO: uncomment log below
    console.log("newChecked:", newChecked);
    setChecked(newChecked);
  };

  const renderUsersData = (data: IUserObject[], searchInputValue: string) => {
    const filteredAndSortedData = data
      .filter((el: IUserObject) =>
        handleFilterSearch([el.last_name, el.first_name], searchInputValue)
      ) //@ts-ignore //TODO: solve this tsLint problem
      .sort((a: IUserObject, b: IUserObject) =>
        //@ts-ignore
        a?.last_name?.localeCompare(b?.last_name)
      );

    const pagesQuantity = Math.ceil(
      filteredAndSortedData.length / POSTS_PER_PAGE
    );

    return filteredAndSortedData.length ? (
      <>
        <ListPagination
          count={pagesQuantity}
          page={page}
          handlePageChange={handlePageChange}
        />
        {filteredAndSortedData
          .splice((page - 1) * POSTS_PER_PAGE, POSTS_PER_PAGE)
          .map((user: IUserObject) => (
            <SingleUser
              key={user.id}
              user={user}
              handleToggle={handleToggle}
              checked={checked}
            />
          ))}
      </>
    ) : (
      <div>
        <p>No users to show</p>
      </div>
    );
  };

  return (
    <List dense className={classes.root}>
      {renderUsersData(data, searchInputValue)}
    </List>
  );
};

export default UserList;
