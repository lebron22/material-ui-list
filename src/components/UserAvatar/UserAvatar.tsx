import React from "react";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";

export interface UserAvatarProps {
  id: number;
  avatar?: string;
}

const UserAvatar: React.FC<UserAvatarProps> = ({ id, avatar }) => {
  return (
    <ListItemAvatar>
      <Avatar alt={`Avatar n°${id + 1}`} src={avatar} />
      {/* {`${first_name.slice(0, 1)}${last_name.slice(0, 1)}`}
      </Avatar> */}
    </ListItemAvatar>
  );
};

export default UserAvatar;
