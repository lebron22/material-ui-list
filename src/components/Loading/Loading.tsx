import React from "react";

export interface LoadingProps {}

const Loading: React.FC<LoadingProps> = () => {
  return <div>Loading data</div>;
};

export default Loading;
