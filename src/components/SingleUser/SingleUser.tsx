import React, { memo } from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import { IUserObject } from "../../interfaces";
import UserAvatar from "../UserAvatar/UserAvatar";

export interface SingleUserProps {
  user: IUserObject;
  checked: any;
  handleToggle: any;
}

const SingleUser: React.FC<SingleUserProps> = ({
  user,
  checked,
  handleToggle,
}) => {
  const { id, first_name, last_name, email, gender, avatar } = user;
  return (
    <ListItem button onClick={handleToggle(id)}>
      <UserAvatar id={id} avatar={avatar} />
      <ListItemText
        id={`${id}`}
        primary={`${first_name} ${last_name} (${gender})`}
        secondary={
          <Typography component="span" variant="body2" color="textPrimary">
            {email}
          </Typography>
        }
      />
      <ListItemSecondaryAction>
        <Checkbox
          edge="end"
          onChange={handleToggle(id)}
          checked={checked.indexOf(id) !== -1}
          inputProps={{ "aria-labelledby": `${id}` }}
        />
      </ListItemSecondaryAction>
    </ListItem>
  );
};

export default memo(SingleUser);
