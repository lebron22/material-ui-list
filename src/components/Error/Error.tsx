import React from "react";

export interface ErrorProps {}

const Error: React.FC<ErrorProps> = () => {
  return <div>Error fetching data</div>;
};

export default Error;
