import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

export const getStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      justifyContent: "center",
      marginBottom: 10,
      "& > *": {
        marginTop: theme.spacing(2),
      },
    },
  })
);
