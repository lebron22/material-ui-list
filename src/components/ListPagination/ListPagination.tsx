import React from "react";
import Pagination from "@material-ui/lab/Pagination";
import { getStyles } from "./getStyles";

export interface ListPaginationProps {
  count: any;
  page: any;
  handlePageChange: any;
}

const ListPagination: React.FC<ListPaginationProps> = ({
  count,
  page,
  handlePageChange,
}) => {
  const classes = getStyles();
  return (
    <div className={classes.root}>
      <Pagination count={count} page={page} onChange={handlePageChange} />
    </div>
  );
};

export default ListPagination;
